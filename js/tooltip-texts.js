var tooltips = '{\
  "addChannel": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Добавьте <br>новый канал</h3>\
            <p class=\'addbar-text-p\'>Обязательно нужно <br>выбрать три параметра:</p>\
            <ul class=\'addbar-text-ul\'>\
              <li>действие</li>\
              <li>цель</li>\
              <li>источник</li>\
            </ul>\
            <p class=\'addbar-text-p\'>Источником может быть аккаунт конкурента, известный блогер, хештег или геометка.</p>\
            <p class=\'addbar-text-p\'>Обозначенные синим <br>цветом параметры <br>необязательны</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "editActionTarget": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Настройте <br>действие и цель</h3>\
            <p class=\'addbar-text-p\'>Действие – это то, что будет делать ваш аккаунт. Цель – по отношению к какой аудитории источника.</p>\
            <p class=\'addbar-text-p\'>Действие и цель – обязательные параметры.</p>\
            <p class=\'addbar-text-p addbar-text-red\'>У вашего аккаунта менее 500 подписчиков и менее 1 месяца с момента первого поста – подписка ограничена.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a><br>\
            <a href=\'#\' class=\'addbar-text-pseudo\'><span>Снять ограничение</span></a>"\
  },\
  "selectSource": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Выберите источник <br>аудитории</h3>\
            <p class=\'addbar-text-p\'>Вам нужно указать аккаунт, хэштег или геометку, аудитория которых вам интересна.</p>\
            <p class=\'addbar-text-p\'>Для выбора источника начните набирать его название в поле и выберите вариант из списка.</p>\
            <p class=\'addbar-text-p\'>Вы можете добавить сразу несколько источников.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "likeLength": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Количество <br>и качество лайков</h3>\
            <p class=\'addbar-text-p\'>При большем количестве лайков вырастает эффективность канала, но снижается охват. При меньшем количестве вы получите больший охват, но меньшую конверсию в новых подписчиков.</p>\
            <p class=\'addbar-text-p\'>Популярные и случайные посты выбираются из последних 30 постов.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "exceptionList": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Список аккаунтов-<br>исключений</h3>\
            <p class=\'addbar-text-p\'>Вы можете загрузить список аккаунтов, который будет исключаться из выполнения задачи.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "optOutMethod": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Выбор метода <br>отписки</h3>\
            <p class=\'addbar-text-p\'>«Все» – отписка от всех подписок, от свежих к старым.</p>\
            <p class=\'addbar-text-p\'>«Не через OML» – отписка от подписок вне сервиса, от свежих к старым.</p>\
            <p class=\'addbar-text-p\'>«Через OML» – отписка от подписок в сервисе, от старых к свежим.</p>\
            <p class=\'addbar-text-p\'>Вы можете указать задержку для подписок «Через OML», чтобы отписываться с периодом ожидания.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "mutualOptions": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Добавьте <br>новый канал</h3>\
            <p class=\'addbar-text-p\'>Обязательно нужно <br>выбрать три параметра:</p>\
            <ul class=\'addbar-text-ul\'>\
              <li>действие</li>\
              <li>цель</li>\
              <li>источник</li>\
            </ul>\
            <p class=\'addbar-text-p\'>Источником может быть аккаунт конкурента, известный блогер, хештег или геометка.</p>\
            <p class=\'addbar-text-p\'>Обозначенные синим <br>цветом параметры <br>необязательны</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "reachOptions": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Добавьте <br>новый канал</h3>\
            <p class=\'addbar-text-p\'>Обязательно нужно <br>выбрать три параметра:</p>\
            <ul class=\'addbar-text-ul\'>\
              <li>действие</li>\
              <li>цель</li>\
              <li>источник</li>\
            </ul>\
            <p class=\'addbar-text-p\'>Источником может быть аккаунт конкурента, известный блогер, хештег или геометка.</p>\
            <p class=\'addbar-text-p\'>Обозначенные синим <br>цветом параметры <br>необязательны</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
}';