(function(){
  $('.left-nav-a').click(function(){
     $('.left-nav-a').removeClass('active');
     $(this).addClass('active');
     var n = $(this).closest('.left-nav-i').index();
     $('.content-loaded').hide().eq(n-1).show();
     $('.side-loaded').hide().eq(n-1).show();
  });

  $('.filter-form-field-inp.dropdown').click(function(e){
  	$('.filter-form-field-inp').removeClass('active');
  	$(this).addClass('active');
  	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  	e.stopPropagation();
  });

  $('.filter-form-field-inp').keypress(function(e){
  	$('.filter-form-field-inp').removeClass('active');
  	$(this).addClass('active loading');
  	e.stopPropagation();
  });

  $('.filter-form-field-dd-i').click(function(e){
  	var t = $(this).html();
    if ($(this).find('span').length > 0) {
      t = t.substr(0, t.indexOf(" <span>"));
    };
  	$(this).closest('.filter-form-field').find('.filter-form-field-inp').val(t).removeClass('active loading');
  	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  	e.stopPropagation();
  });

  $('body').click(function(e){
  	$('.filter-form-field-inp').removeClass('active loading');
  	e.stopPropagation();
  });

  $('.modal-filter-tabs-nav-a').click(function(e){
    var n = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    $(this).closest('.modal-filter').find('.modal-filter-tab').hide().eq(n).show();
    if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  });
})();