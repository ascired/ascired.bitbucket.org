var tooltips = '{\
  "addChannel": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Добавьте <br>канал привлечения</h3>\
            <p class=\'addbar-text-p\'>Вам нужно выбрать три обязательных параметра:</p>\
            <ul class=\'addbar-text-ul\'>\
              <li>действие</li>\
              <li>цель</li>\
              <li>источник</li>\
            </ul>\
            <p class=\'addbar-text-p\'>Источником может быть аккаунт конкурента, известный блогер, хештег или геометка на карте).</p>\
            <p class=\'addbar-text-p\'>Остальные параметры, обозначенные синим цветом, являются необязательными.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "editActionTarget": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Настройте <br>действие и цель</h3>\
            <p class=\'addbar-text-p\'>Действие – это то, что будет делать ваш аккаунт. Цель – по отношению к какой аудитории источника.</p>\
            <p class=\'addbar-text-p\'>Действие и цель – обязательные параметры.</p>\
            <p class=\'addbar-text-p addbar-text-red\'>У вашего аккаунта менее 500 подписчиков и менее 1 месяца с момента первого поста – подписка ограничена.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a><br>\
            <a href=\'#\' class=\'addbar-text-pseudo\'><span>Снять ограничение</span></a>"\
  },\
  "selectSource": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Выберите источник <br>аудитории</h3>\
            <p class=\'addbar-text-p\'>Вам нужно указать аккаунт, хэштег или геометку, аудитория которых вам интересна.</p>\
            <p class=\'addbar-text-p\'>Для выбора источника начните набирать его название в поле и выберите вариант из списка.</p>\
            <p class=\'addbar-text-p\'>Вы можете добавить сразу несколько источников.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "likesAmount": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Количество <br>и качество лайков</h3>\
            <p class=\'addbar-text-p\'>При большем количестве лайков вырастает эффективность канала, но снижается охват. При меньшем количестве вы получите больший охват, но меньшую конверсию в новых подписчиков.</p>\
            <p class=\'addbar-text-p\'>Популярные и случайные посты выбираются из последних 30 постов.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "exceptionList": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Список аккаунтов-<br>исключений</h3>\
            <p class=\'addbar-text-p\'>Вы можете загрузить список аккаунтов, который будет исключаться из выполнения задачи.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "optOutMethod": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Выбор метода <br>отписки</h3>\
            <p class=\'addbar-text-p\'>«Все» – отписка от всех подписок, от свежих к старым.</p>\
            <p class=\'addbar-text-p\'>«Не через OML» – отписка от подписок вне сервиса, от свежих к старым.</p>\
            <p class=\'addbar-text-p\'>«Через OML» – отписка от подписок в сервисе, от старых к свежим.</p>\
            <p class=\'addbar-text-p\'>Вы можете указать задержку для подписок «Через OML», чтобы отписываться с периодом ожидания.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "mutualOptions": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Выбор условия <br>взаимности</h3>\
            <p class=\'addbar-text-p\'>Вы можете указать, от каких подписок вам отписаться – от всех, от тех, кто взаимно подписан на вас или от тех, кто не подписан в ответ.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "reachOptions": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Настройки охвата <br>аудитории</h3>\
            <p class=\'addbar-text-p\'>Здесь вы можете указать параметров аудитории, которую будете искать среди аудитории источника.</p>\
            <p class=\'addbar-text-p\'>Обратите внимание – некоторые из параметров очень сильно сужают охват.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a><br>\
            <a href=\'#\' class=\'addbar-text-pseudo\'><span>Рекомендуемые параметры</span></a>"\
  },\
  "limitSubscribers": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Минимум и максимум <br>подписчиков</h3>\
            <p class=\'addbar-text-p\'>Минимум стоить указывать для отсечения ботов, а максимум – для отсечения аудитории, которая не заметит ваши действия из-за большого количества взаимодействий со стороны своих подписчиков.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "limitSubscribes": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Минимум и максимум <br>подписок</h3>\
            <p class=\'addbar-text-p\'>Минимум стоить указывать для отсечения ботов, а максимум – для отсечения аудитории с большим количеством подписок, которая не будет обращать внимания на ваш контент из-за большого количества публикаций, или же сама использует массфолловинг.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "privateProfile": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Приватность <br>профиля</h3>\
            <p class=\'addbar-text-p\'>Вы можете выбрать, на какую аудиторию вам подписываться – с открытым профилем или закрытым профилем.</p>\
            <p class=\'addbar-text-p\'>По умолчанию вы будете подписываться на все типы аудитории.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "lastPost": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Время последней <br>публикации</h3>\
            <p class=\'addbar-text-p\'>Публикация в течение последнего месяца говорит о нормальной активности целевого аккаунта, но стоит помнить об огромном количестве пользователей, публикующих фото реже.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "postsAmount": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Количество <br>постов</h3>\
            <p class=\'addbar-text-p\'>Минимум стоит указывать <br>для отсечения ботов.</p>\
            <p class=\'addbar-text-p\'>Мы рекомендуем указывать количество не менее 13 – это позволит отсечь значительное количество «лендинговых» арбитражных аккаунтов – обычно они публикуют 9 или 12 фото.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "profilePhoto": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Фотография <br>профиля</h3>\
            <p class=\'addbar-text-p\'>Наличие фото профиля стоит указывать для отсечения ботов – обычно они не заполняют профиль личными данными.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "hasLink": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Наличие ссылки <br>в профиле</h3>\
            <p class=\'addbar-text-p\'>Опция позволяет отсекать значительное количество коммерческих аккаунтов, указывающих ссылку на сайт или на лендинг партнёрской программы.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "hasPhone": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Наличие номера <br>телефона</h3>\
            <p class=\'addbar-text-p\'>Вы можете исключить аккаунты с номером телефона (а также WhatsApp и Viber) в профиле.</p>\
            <p class=\'addbar-text-p\'>Это позволяет отсекать значительное количество коммерческих аккаунтов.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "languageFilter": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Лингвистическая <br>фильтрация</h3>\
            <p class=\'addbar-text-p\'>Здесь вы можете отфильтровать целевую аудиторию по языковым настройкам, а также указать лист стоп-слов.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "isCyrillic": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Добавьте <br>канал привлечения</h3>\
            <p class=\'addbar-text-p\'>Мы проверяем наличие символов в имени, описании и последних 30 публикациях пользователя.</p>\
            <p class=\'addbar-text-p\'>Вам не нужно указывать «Кириллицу», если в качестве цели вы указали кириллический хэштег.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "isArabic": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Арабская <br>письменность</h3>\
            <p class=\'addbar-text-p\'>Мы проверяем наличие арабские символов в имени, описании или последних 30 постах пользователя.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "isTurkish": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Турецкая <br>письменность</h3>\
            <p class=\'addbar-text-p\'>Мы проверяем наличие тюркских символов в имени, описании или последних 30 постах пользователя.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "stopException": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Исключение списка <br>стоп-слов</h3>\
            <p class=\'addbar-text-p\'>Вы можете загрузить список стоп-слов. Профили, в которых будет найдено точное совпадение со словом из стоп-списка, будут исключены из поиска.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "additionalOptions": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Дополнительные <br>условия поиска</h3>\
            <p class=\'addbar-text-p\'>Вы можете работать с максимально таргетированной целевой аудиторией.</p>\
            <p class=\'addbar-text-p\'>Внимание – дополнения сильно сужают охват и могут значительно увеличить время поиска, а при неверно заданных настройках показать нулевой результат.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "additionalHash": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Дополнительные <br>хэштеги</h3>\
            <p class=\'addbar-text-p\'>Вы найдёте пользователей, у которых есть публикации с сочетанием основного источника и одного из хэштегов, указанных в дополнениях.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "additionalGeo": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Дополнительные <br>геометки</h3>\
            <p class=\'addbar-text-p\'>Вы найдёте пользователей, у которых есть публикации с сочетанием основного источника и одной из геометок, указанных в дополнениях.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "accountsUnique": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Уникальность <br>аккаунтов</h3>\
            <p class=\'addbar-text-p\'>Мы автоматически исключаем из поиска аккаунты, с которыми вы уже контактировали.</p>\
            <p class=\'addbar-text-p\'>Если вы выберите опцию «Все аккаунты», вы сможете повторно лайкать или подписываться на выбранную аудиторию.</p>\
            <a href=\'#\' class=\'addbar-text-a\'>Подробнее</a>"\
  },\
  "defaultState": {\
    "str": "<h3 class=\'addbar-text-ttl\'>Редактирование <br>канала привлечения</h3>\
            <div class=\'addbar-section\'>\
              <p class=\'addbar-last-date\'>30 янв 2014 в 23:59</p>\
              <p class=\'addbar-state state-green\'>В процессе</p>\
            </div>\
            <div class=\'addbar-section\'>\
              <p>подписаться и лайкать комментарии</p>\
              <a href=\'#\' class=\'addbar-account-link\'>nationalgeographiс</a>\
            </div>\
            <div class=\'addbar-section addbar-statistic\'>\
              <p>68 905 взаимодействий</p>\
              <p>4 800 охват</p>\
              <p>2.51% эффективность</p>\
              <p>442 подписчика</p>\
            </div>"\
  }\
}';

var tooltipsObj = JSON.parse(tooltips);

(function(){
  $('.addbar-switch').click(function(){
    if (!$(this).hasClass('addbar-tags')) {

      if(!$(this).hasClass('active')) {
        $('.addbar-switch').removeClass('active');
        $(this).addClass('active');
        var tar = $(this).data('target');
        var str = tooltipsObj[tar].str;
        $('.addbar').hide();
        $('.addbar-text').show();
        $('.addbar-text>div').html(str);
        $('.new-channel-right-inner').removeClass('addbar-default-state');
      } else {
        $('.addbar-switch').removeClass('active');
        $('.addbar').hide();
        var str = tooltipsObj['defaultState'].str;
        $('.addbar-text').show();
        $('.addbar-text>div').html(str);
        $('.new-channel-right-inner').addClass('addbar-default-state');
      } 
      $(document).trigger('formResize');
    };
  });
})();