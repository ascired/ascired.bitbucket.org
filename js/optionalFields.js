(function(){
  $('.optional-field-inp').click(function(){
    var t = $(this).data('optional');
    $(this).closest('.content-tab').find('.filter-form-elem').not('.field-all').not($(this).closest('.filter-form-elem')).hide();
    $('.field-' + t).show();
    $(document).trigger('formResize');
  });
})();