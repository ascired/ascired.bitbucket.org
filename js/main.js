(function ( $ ) { 
	jQuery.fn.extend({
            entireHeight: function() {
                var w = $(window).height();
                var viewportOffset = this[0].getBoundingClientRect();
                var ot = viewportOffset.top;
                this.css('height', w-ot);
            }
        });
	$.fn.fitModalHeight = function() {
		this.each(function(){
			$(this).removeAttr('style');
			if ($(this).closest('.modal-window').css('top') != '90px') {
				var modalHeight = $(this).closest('.modal-window').height();
				var modalPadTop = $(this).closest('.modal-window').css('padding-top');
				modalPadTop = parseInt(modalPadTop.replace('px', ''));
				var ot = $(this).offset().top;
				var h = modalHeight - ot + modalPadTop;
				$(this).css('height', h);
				$(this).mCustomScrollbar("update");
			};
		});
	};
	$.fn.fillEntire = function() {
		this.each(function(){
			var holder = $(this).closest('.new-channel-content-holder');
			var	ot = $(this).closest('.filter-form-elem').offset().top;
			var holderOt = holder.offset().top;
			var holderHeight = holder.height();
			var h = holderHeight + holderOt - ot - 64;
			$(this).css('height', h);
			$(this).mCustomScrollbar("update");
		});
	};

	$.fn.focusOn = function(target){
		var newFocus;
		this.closest('.filter-form-field-dd').mCustomScrollbar("stop");
		if (target === 'next') {
			if (this.is(':last-child')) {
				newFocus = this.siblings().first();
			} else {
				newFocus = this.next();
			};
		} else if (target === 'prev') {
			if (this.is(':first-child')) {
				newFocus = this.siblings().last();
			} else {
				newFocus = this.prev();
			};
		} else if (this.closest('.filter-form-field').find('.filter-form-field-dd').find('.filter-form-field-dd-i').length > 0) {
			newFocus = this.closest('.filter-form-field').find('.filter-form-field-dd').find('.filter-form-field-dd-i').first();
		} else {
			newFocus = this;
		};
		newFocus.focus();
		var ot = newFocus[0].offsetTop;
		this.closest('.filter-form-field-dd').mCustomScrollbar("scrollTo", ot, {scrollInertia:100, timeout:100});
	};
}( jQuery ));

function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if(pair[0] == variable){return pair[1];}
	}
	return(false);
}
function showModal(target) {
	$('.modal-window#'+target).show().css('display','inline-block');
	$('.modal-window-holder').addClass('centered');
};
function updateScrollbar(target) {
	target.mCustomScrollbar("update");
}
$(document).ready(function(){
	$('.js-entireHeight').entireHeight();
});
$(window).resize(function(){
	$('.js-entireHeight').entireHeight();
});
$('.filter-form-field-dd-i').keydown(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
  if (code == 40) { 				//down
  	$(this).focusOn('next');
  } else if (code == 38) { 	//up
  	$(this).focusOn('prev');
  } else if (code == 13) { 	//enter

  } else if (code == 27) { 	//esc
  	$(this).closest('.filter-form-field').find('.filter-form-field-inp').removeClass('active').focus();
  	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  } else if (code == 9) { 	//tab
  	$(this).focusOn('next');
  	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  }
});
$('.filter-form-field-inp').keydown(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 40) { 				//down
		$(this).focusOn();
		if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  } else if (code == 27) { 	//esc
  	$(this).removeClass('active loading').focus();
  	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
  } else if (code == 13) { //enter
  	if (!$(this).hasClass('suggest')) {
  		var flag = $(this).hasClass('active');
  		if ($(this).parents('.filter-form-field').length < 2) {
  			$('.filter-form-field-inp').removeClass('active');
  		};
  		if (!flag) {
  			$(this).addClass('active');
  		};
  	};
  };
  e.stopPropagation();
});

$('.filter-form-field-dd-i').hover(function(){
	$(this).focus();
});

if ($('.filter-form-location').length > 0) {
	$('.filter-form-location').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 50},
		advanced:{
			updateOnContentResize: true
		}
	});
};
if ($('.filter-form-additional-body').length > 0) {
	$('.filter-form-additional-body').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 50},
		advanced:{
			updateOnContentResize: true
		}
	});
};
if ($('.filter-form-field-dd').not('.full-size').length > 0) {
	$('.filter-form-field-dd').not('.full-size').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 50},
		advanced:{
			updateOnContentResize: true
		}
	});
};
if ($('.my-accounts-list').length > 0) {
	$('.my-accounts-list').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 50},
		advanced:{
			autoScrollOnFocus: false,
			updateOnContentResize: true
		}
	});
};

if ($('.secret-scroll').length > 0) {
	$('.secret-scroll').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 150},
		autoHideScrollbar: true,
		advanced:{
			autoScrollOnFocus: false,
			updateOnContentResize: true
		}
	});
};

if ($('.new-channel-content-holder.custom-scroll').length > 0) {
	$('.new-channel-content-holder.custom-scroll').mCustomScrollbar({
		keyboard: false,
		mouseWheel:{scrollAmount: 150},
		autoHideScrollbar: false,
		advanced:{
			autoScrollOnFocus: false,
			updateOnContentResize: true
		}
	});
};
$('.fit-height').fitModalHeight();
$('.fill-entire').fillEntire();

$(window).resize(function(){
	$('.fit-height').fitModalHeight();
	$('.fill-entire').fillEntire();
});

$('.modal-show').click(function(e){
	var t = $(this).data('target');
	showModal(t);  
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.modal-close').click(function(e){
	var t = $(this).closest('.modal-window').attr('id');
	$('.modal-window').hide();
	$('[data-target="'+t+'"]').removeClass('active');
	$('.modal-window-holder.centered').removeClass('centered');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.modal-window-holder.centered').click(function(e) {
	$('.modal-close').trigger('click');
	e.preventDefault();
	e.stopPropagation();
});

$('.modal-window').click(function(e) {
	e.stopPropagation();
});

$('.modal-toggle').click(function(e){
	var t = $(this).data('target');
	if ($('.modal-window#'+t).is(':visible')) 
	{
		$('.modal-window').hide();
	} else {
		$('.modal-window#'+t).show();
	};
	$(this).toggleClass('active');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.switch-link').click(function(e){
	var t = $(this).text();
	$(this).toggleClass('active');
	$(this).text($(this).attr('data-alt'));
	$(this).attr('data-alt',t);
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.smooth-slide').click(function(e) {
	var str = $(this).attr("href");
	str = str.substr(1,str.length);
	var target = $('#'+str);
	if (str.length<1) {target = $('body')};
	$('body,html,.page-inner').animate({
		scrollTop: target.offset().top - 57
	}, 800);
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.toggle-bubble').click(function(e){
	$(this).closest('.content-ttl, .title').find('.content-bubble').toggle();
	$(this).closest('.content-ttl, .title').find('.content-ttl-q.toggle-bubble, .title__q.toggle-bubble').toggle();
	var layoutHead = $('.layout__header').outerHeight();
	$('.layout__pad-top').css('padding-top',layoutHead);
	$(document).trigger('resize');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.filter-form-extend').click(function(e){
	$(this).toggleClass('active');
	$('.filter-content .filter-form-row.also').slideToggle(300);
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.filter-selected-i').click(function(e){
	$(this).remove();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.filter-selected-clear').click(function(e){
	$('.filter-selected-i').remove();
	$('.filter-content-form').find('.filter-form-field-inp').val('');
	$(this).removeClass('active');
	$('.filter-content .filter-form-row.also').slideUp(300);
	$('.filter-selected').hide();
	$(document).trigger('resize');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.left-nav-a').click(function(){
	$('.left-nav-a').removeClass('active');
	$(this).addClass('active');
	var n = $(this).closest('.left-nav-i').index();
	$('.content-loaded').hide().eq(n).show();
	$('.side-loaded').hide().eq(n).show();
});

$('.filter-form-field-inp').click(function(){
	var e = jQuery.Event("keydown");
	e.which = 13;
	$(this).trigger(e);
});

$('.filter-form-field-dd-btn').click(function(e){
	var flag = $(this).closest('.filter-form-field').find('.filter-form-field-inp').hasClass('active');
	$('.filter-form-field-inp').removeClass('active');
	if (!flag) {
		$(this).closest('.filter-form-field').find('.filter-form-field-inp').addClass('active');
	};
	$(this).closest('.filter-form-field').find('.filter-form-field-inp').focusOn();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
	e.stopPropagation();
});

$('.filter-form-field-inp.suggest').keypress(function(e){
	$('.filter-form-field-inp').removeClass('active loading');
	$(this).addClass('active loading');
	e.stopPropagation();
});

$('.filter-form-field-dd-i').click(function(e){
	var t = $(this).find('.filter-form-field-val').text();
	var field = $(this).closest('.filter-form-field');
	field.find('input.filter-form-field-inp').val(t);
	field.find('.filter-form-field-inp').removeClass('active loading').focus();
	if (field.find('div.filter-form-field-inp.ghost').length > 0) {
		t = $(this).html();
		var da = $(this).data('style');
		field.find('div.filter-form-field-inp.ghost').html(t);
		$(this).closest('.filter-form-elem-wrap').attr('data-style', da);
	};
	if ($(this).closest('.filter-form-elem').find('.filter-form-lbl-chk').length > 0) {
		$(this).closest('.filter-form-elem').find('.filter-form-lbl-chk').prop('checked', true);
	};
	if (!$(this).hasClass('js-default-value')) {
		$(this).closest('.filter-content-inner').addClass('selected-state');
	} else {
		$(this).closest('.filter-content-inner').removeClass('selected-state');
	}
	e.preventDefault();
	e.stopPropagation();
});

$('.filter-form-field-custom-btn').click(function(e){
	var t = $(this).prev('.filter-form-field-custom-val').val();
	$(this).closest('.filter-form-field').find('.filter-form-field-inp').val(t).removeClass('active loading');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.filter-form-field-dd-line, .dropdown, .accounts-tools-more').click(function(e){
	e.stopPropagation();
});

$('body').click(function(e){
	$('.filter-form-field-inp').removeClass('active loading');
	$('.accounts-i-tools').removeClass('active');
	e.stopPropagation();
});
$('body').keydown(function(e){
	if (e.which == 27) {
		$('div.filter-form-field-inp').removeClass('active loading');
		e.stopPropagation();
	};
});

$('.modal-filter-tabs-nav-a').click(function(e){
	var n = $(this).index();
	$(this).addClass('active').siblings().removeClass('active');
	$(this).closest('.modal-window').find('.modal-filter-tab').hide().eq(n).show();
	$(this).closest('.modal-window').find('.modal-filter-tab .fit-height').fitModalHeight();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.content-tabs-nav-a').click(function(e){
	var n = $(this).index();
	if (typeof $(this).attr('data-target') !== 'undefined') {
		var t = $(this).attr('data-target');
		n = $('#' + t).index();
	};
	$(this).addClass('active').siblings().removeClass('active');
	$(this).closest('.content-holder').find('.content-tab').hide().eq(n).show();
	$(this).closest('.content-holder').find('.new-channel-content-holder .fill-entire').fillEntire();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.filter-form-taglist-switch').click(function(e){
	$(this).toggleClass('active').closest('.filter-form-taglist').find('.filter-form-taglist-dd').toggle();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.accounts-tools-more').click(function(e){
	$('.accounts-i-tools').removeClass('active');
	$(this).closest('.accounts-i-tools').addClass('active');
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.settings-head-ttl').click(function(e){
	$(this).closest('.settings-head').toggleClass('active').next('.settings-body').slideToggle();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.addbar-tags').click(function(e) {
	if(!$(this).hasClass('active')) {
		$('.addbar-switch').removeClass('active');
		$(this).addClass('active');
		$('.addbar').hide();
		$('.addbar-list').show();
	} else {
		$('.addbar-switch').removeClass('active');
		$('.addbar').hide();
		$('.addbar-list').hide();
		$('.addbar-text').show();
		$(document).trigger('formResize');
	}
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.addbar-none').click(function(e) {
	$('.new-channel-right').hide();
	$('.new-channel-content-holder').addClass('new-channel-content-holder_full');
});
$('.addbar-default').click(function(e) {
	$('.new-channel-right').show();
	$('.new-channel-content-holder').removeClass('new-channel-content-holder_full');
});

$('.optional-trigger').click(function(e) {
	var t = $(this).data('target');
	$('.optional-view').hide();
	$('.optional-view_' + t).show();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.optional-reset').click(function(e) {
	$('.optional-view').hide();
	$('.optional-view_default').show();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});

$('.fake-file-upload').click(function(e){
	$(this).prev('.fake-file-input').click();
	if(e.preventDefault) e.preventDefault(); else e.returnValue = false;
});
$('.fake-file-input').change(function(e){
	
	if ($(this).val() !== '') {
		$(this).closest('.fake-file').find('.fake-file-ready').removeAttr('disabled');
	};	
});

$('.inp-min-max-value').blur(function() {
	var t = $(this).data('extremum');
	var v = $(this).val();
	var str = v + '<span> – ' + t + '</span>';
	var elem = $('<div class="filter-form-field-inp-mask">').html(str);
	if ((v !== '')&&($(this).closest('.filter-form-field-inner').find('.filter-form-field-inp-mask').length < 1)) {
		$(this).before(elem);
	} else if ($(this).closest('.filter-form-field-inner').find('.filter-form-field-inp-mask').length > 0) {
		$(this).closest('.filter-form-field-inner').find('.filter-form-field-inp-mask').html(v).show();
	};
});
$('.inp-min-max-value').focus(function() {
	$(this).prev('.filter-form-field-inp-mask').hide();
});

function toggleRightbar() {
	if($('.new-channel-content-holder').hasClass('new-channel-content-holder_full')) {
		$('.new-channel-right').show();
		$('.new-channel-content-holder').removeClass('new-channel-content-holder_full');
	} else {
		$('.new-channel-right').hide();
		$('.new-channel-content-holder').addClass('new-channel-content-holder_full');
	}
}

function fieldHalf() {
	var t = $('.filter-form-elem_half:visible:first');
	var w = Math.floor((t.closest('.filter-form').width() - 22 - t.find('.filter-form-lbl').outerWidth())/2) + t.find('.filter-form-lbl').outerWidth() + 23;
	$('.filter-form-elem_half').outerWidth(w);
};
fieldHalf();

$(document).on('formResize', function() {
	fieldHalf();
});

$(window).resize(function() {
	fieldHalf();
})

$('.rside-toggle').click(function(e) {
	$('.rside-toggle').toggleClass('active');
	$('.layout__main').toggleClass('rside-visible');
});

$('.filter-form-clear-a').click(function(e) {
	e.preventDefault();
	$(this).closest('.filter-content-form').find('.filter-form-field-inp').val('');
});

function copyRef(e) {
	var
	inp = $('.js-reflink-inp').clone();
	inp.removeClass('has-ghost').appendTo('body');
	inp.select();
	try {
		document.execCommand('copy');
		inp.blur();
	}
	catch (err) {
		alert('Нажмите Ctrl/Cmd+C чтобы скопировать');
	}
	inp.remove();
}

function changeRef(e) {
	var
	val = $('.js-reflink').text().trim();
	$('.js-reflink-inp').val(val);
}

$('.js-reflink-copy').click(function(e) {
	copyRef(e);
});

$('.js-reflink-change').click(function(e) {
	changeRef(e);
});


if (!!$('.layout__header_fixed').length) {
	var oH = $('.layout__header_fixed').outerHeight();
	$('.layout__pad-top').css('padding-top', oH);
}

// $(window).scroll(function() {
// 	if ($('body').hasClass('native-scroll')) {
// 		var scroll = $(window).scrollTop();
// 		var maxHeight = $('.layout__main').height() - $(window).height();
// 		var sidebars = $('.layout__lside, .content__account, .layout__rside');
// 		if (scroll > maxHeight) {
// 			scroll = maxHeight;
// 		}
// 		sidebars.css('margin-top', scroll);
// 	}
// });