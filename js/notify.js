function notifySlickInit(tab) {
    $('.slick-initialized').slick('unslick');
    $('.js-slick-begin').hide();
    var slick = $(tab).find('.js-slick-init');
    if (slick.length) {
        var ntf = (slick.height() / 64 >> 0);
        slick.slick({
            vertical: true,
            dots: false,
            arrows: true,
            slidesToShow: ntf,
            slidesToScroll: ntf,
            infinite: false,
            nextArrow: $('.js-slick-next'),
        });                
    }
}

$(document).ready(function() {
    var tab = $('.rside__tab').eq(0);
    notifySlickInit(tab);
});
$(document).on('rsideTabToggle', function(e, tab) {
    notifySlickInit(tab);
});
$('.js-slick-init').on('afterChange', function(event, slick, currentSlide){
    if (currentSlide !== 0) {
        $(this).closest('.notification__wrap').find('.js-slick-begin').show();
    }
});
$('.js-slick-begin').click(function(e) {
    $(this).closest('.notification__wrap').find('.notification__holder').slick('slickGoTo', 0);
    $(this).hide();
    e.preventDefault();
});

$('.js-rside-close').click(function(e) {
    $('.rside_main').removeClass('hidden-state');
    $(this).closest('.rside_secondary').removeClass('visible-state').addClass('hidden-state');
    // $('.rside-toggle').removeClass('active');
    // $('.layout__main').removeClass('rside-visible');
    e.preventDefault();
});