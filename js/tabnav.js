function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
  return(false);
}
$('.page__nav').click(function(e) {
  e.preventDefault();
  var n = $(this).index();
  $(this).addClass('active').siblings().removeClass('active');
  $(this).closest('.page__navtabs').find('.page__tab').hide().eq(n).show();
  $('.page__tab').eq(n).trigger('tabChangedEvent');
  if ($(this).closest('.modal-window').length <= 0) {
    window.history.pushState(null, null, '?navtab=' + parseInt(n+1));
  }
  $(document).trigger('formResize');
});
$('.js-goto-tab').click(function(e) {
  e.preventDefault();
  var n = $(this).data('page-tab');
  $(this).closest('.page__navtabs').find('.page__tab').hide().eq(n-1).show();
});
var tab = getQueryVariable('navtab');
if (tab==false) {
  $('.page__nav').filter('.active').trigger('click');
} else {
  $('.page__nav').eq(tab-1).trigger('click');
};


$('.rside__navtab').click(function(e) {
  e.preventDefault();
  var n = $(this).index();
  var tabs = $(this).closest('.layout__rside').find('.rside__tab');
  $(this).addClass('active').siblings().removeClass('active');
  tabs.hide().eq(n).show();
  if (tabs.eq(n).find('.js-entireHeight').length) {
    tabs.eq(n).find('.js-entireHeight').entireHeight();
  }
  $(document).trigger('rsideTabToggle', tabs.eq(n));
});