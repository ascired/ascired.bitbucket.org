var acCount = ($('.accounts__inner').height() / 60 >> 0);
$('.accounts__inner').height() % 60 >= 40 ? acCount = acCount + 1 : acCount;
$('.accounts__inner').slick({
    vertical: true,
    dots: false,
    arrows: true,
    slidesToShow: acCount,
    slidesToScroll: acCount,
    infinite: true,
    nextArrow: $('.accounts__more'),
});